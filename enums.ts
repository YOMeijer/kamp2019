export enum WidgetTypes {
	weather = 0,
	nefit = 1,
	tempHistory = 2,
	sonos = 5,
	lightSwitch = 10
}

export enum HistoryTypes {
	inHouseTempDay = 0,
	outsideTempDay = 1,
	supplyTempDay = 2
}
