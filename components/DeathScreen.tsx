import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { PlayerState } from "../reducers/PlayerReducer";
import RankIcon from "./RankIcon";
import { msToTime } from "../shared/shared.functions";
import { players } from "../Firebase";
import { User } from "firebase";
import { AppStore } from "../reducers";

type ThisState = {
  deathTimer: any;
  timeToAlive: string;
};

type ThisProps = {
  player: PlayerState;
  auth: User;
};

class DeathScreen extends React.Component<ThisProps, ThisState> {
  componentWillMount() {
    this.setState({
      deathTimer: setInterval(() => {
        this.checkTimeOfDeath();
      }, 1000)
    });
  }

  componentWillReceiveProps(nextProps: ThisProps) {}

  componentWillUnmount() {
    clearInterval(this.state.deathTimer);
    this.setState({ deathTimer: null });
  }

  checkTimeOfDeath() {
    const now = new Date().getTime();
    if (now - this.props.player.timeOfDeath > 1000 * 60 * 5) {
      players
        .child(this.props.auth.uid)
        .child("playerInfo")
        .update({ isDead: false });
    }
    this.setState({
      timeToAlive: msToTime(
        1000 * 60 * 5 - (now - this.props.player.timeOfDeath)
      )
    });
  }

  render() {
    return (
      <div>
        <div className="flx-col">
          <div>Verslagen door:</div>
          <div>{this.props.player.killedBy}</div>
          <RankIcon rank={this.props.player.killedByRank} size="100px" />
          <br />
          <div>{this.state.timeToAlive ? this.state.timeToAlive : null}</div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state: AppStore) {
  return {
    player: state.player,
    auth: state.auth
  };
}

export default connect(
  mapStateToProps,
  {}
)(DeathScreen);
