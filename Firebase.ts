import firebase from "firebase/app";
import "firebase/database";

var firebaseConfig = {
  apiKey: "AIzaSyA79vxucUEQQdNhkKasd_TBl4RuQPbYdD4",
  authDomain: "welpen-2018.firebaseapp.com",
  databaseURL: "https://welpen-2018.firebaseio.com",
  projectId: "welpen-2018",
  storageBucket: "welpen-2018.appspot.com",
  messagingSenderId: "241538493429",
  appId: "1:241538493429:web:2e2ba095e34a095c"
};
firebase.initializeApp(firebaseConfig);

export const database = firebase.database().ref();
export const kamp2019 = database.child("kamp2019");
