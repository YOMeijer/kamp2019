import { kamp2019 } from "../Firebase";
import * as _ from "lodash";

export const SET_DATA = "set_data";

export function dataListener() {
  return dispatch => {
    kamp2019.on("value", data => {
      const obj = data.val();

      dispatch({
        type: SET_DATA,
        payload: _.map(obj.teams, (team, index) => {
          return { name: index, data: team };
        })
      });
    });
  };
}
