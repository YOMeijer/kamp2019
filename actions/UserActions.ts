import { auth } from "../Firebase";

// export const SET_USER_AUTH = "set_user_auth";

// export function authState() {
//   return dispatch => {
//     auth.onAuthStateChanged(user => {
//       dispatch({
//         type: SET_USER_AUTH,
//         payload: user
//       });
//     });
//   };
// }

// auth
// 	.signInWithEmailAndPassword("yurimeijer@xs4all.nl", "tttttt")
// 	.then(res => {
// 		console.log(res.user.uid);
// 		// this.props.setUserInfo({ uid: res.user.uid });
// 		// this.props.getPlayerInfo(res.user.uid);
// 	})
// 	.catch(function(error) {
// 		// Handle Errors here.
// 		var errorCode = error.code;
// 		var errorMessage = error.message;

// 		console.error(errorCode);
// 		console.error(errorMessage);
// 		// ...
// 	});

// export const SET_PLAYER_INFO = "set_player_info";

// export function playerInfoListener(uid: string) {
//   console.log(uid);
//   return dispatch => {
//     players
//       .child(uid)
//       .child("playerInfo")
//       .on("value", data => {
//         const obj = data.val();
//         if (!obj) {
//           const playerState: PlayerState = {
//             team: 0,
//             type: 0,
//             isDead: false,
//             rank: 2,
//             name: "Nieuwe speler",
//             killedBy: null,
//             killedByRank: null,
//             timeOfDeath: 0
//           };

//           players.child(uid).set({ playerInfo: playerState });
//         } else {
//           dispatch({
//             type: SET_PLAYER_INFO,
//             payload: obj
//           });
//         }
//       });
//   };
// }

// export const GET_USER = "get_user";

// export function getUser() {
//   return dispatch => {
//     auth.onAuthStateChanged(user => {
//       if (user != null) {
//         // getUserInfo(user.uid, user.email);
//       }

//       dispatch({
//         type: GET_USER,
//         payload: user
//       });
//     });
//   };
// }

// export const GET_USER_INFO = "get_user_info";
// export const RESET_USER_INFO = "reset_user_info";

// export function getUserInfo(uid, email) {
// 	var ref = users.child(uid);

// 	if (uid != undefined && email != undefined) {
// 		return dispatch => {
// 			ref.on("value", data => {
// 				const obj = data.val();

// 				if (obj === null) {
// 					newUsers.child(uid).set({ email: email });

// 					dispatch({
// 						type: GET_USER_INFO,
// 						payload: null
// 					});
// 				} else {
// 					dispatch({
// 						type: GET_USER_INFO,
// 						payload: obj
// 					});
// 				}
// 			});
// 		};
// 	}
// }

// export const GET_NEW_USERS = 'get_new_users';

// export function getNewUsers() {

// 	return dispatch => {
// 		newUsers.on('value', data => {
// 			const obj = data.val();

// 			dispatch({
// 				type: GET_NEW_USERS,
// 				payload: obj
// 			});
// 		});
// 	}
// }

// export const GET_USERLIST = 'get_userlist';

// export function getUserList() {

// 	return dispatch => {
// 		userList.on('value', data => {

// 			const obj = data.val();

// 			dispatch({
// 				type: GET_USERLIST,
// 				payload: obj
// 			});
// 		});
// 	}
// }

// export const LOGIN_ERROR = 'login_error';

// export function login(email, password) {
// 	return dispatch => {
// 		auth.signInWithEmailAndPassword(email, password).catch(err => {

// 			dispatch({
// 				type: LOGIN_ERROR,
// 				payload: err
// 			}); });
// 	}

// }

// export function logout() {
// 	return dispatch => {

// 		auth.signOut();
// 	}
// }

// export function signUpWithEmail(email, password) {
// 	return dispatch => auth.createUserWithEmailAndPassword(email, password);
// }

// export const GET_PERSONAL_LOG = 'get_personal_log';

// export function getPersonalLog(id) {
// 	return dispatch => {
// 		log.child(id).once('value', data => {
// 			const obj = data.val();

// 			dispatch({
// 				type: GET_PERSONAL_LOG,
// 				payload: obj
// 			});
// 		});
// 	}
// }

// export const GET_VERSION = 'get_version';

// export function getVersion(id) {
// 	return dispatch => {
// 		version.once('value', data => {
// 			const obj = data.val();

// 			dispatch({
// 				type: GET_VERSION,
// 				payload: obj
// 			});
// 		});
// 	}
// }
