import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { AppStore } from "../reducers";
import { dataListener } from "../actions/SystemActions";
import { SystemState } from "../reducers/SystemReducer";

const meterImage = require("../static/img/meter.png");

type ThisState = {
	data: SystemState;
};

type ThisProps = {
	dataListener();
	data: SystemState;
};

class ScoreContainer extends React.Component<ThisProps, ThisState> {
	state = {
		data: null
	};

	componentDidMount() {
		this.props.dataListener();
	}

	static getDerivedStateFromProps(props, state) {
		return state;
	}

	renderTeams() {
		return _.map(this.props.data, team => {
			return (
				<div className={"team-scorebord-container " + team.name} key={team.name}>
					<div
						className={"co2" + (team.data.score.co2 > 70 ? " fade-error" : " fade-slow")}
						style={{
							height: team.data.score.co2 * 0.8 + "%",
							background:
								team.data.score.co2 < 30
									? "#2d8f19"
									: team.data.score.co2 < 50
									? "#ffde59"
									: team.data.score.co2 < 70
									? "orange"
									: "red"
						}}
					/>
					<div
						className={"co4" + (team.data.score.co4 > 70 ? " fade-error" : " fade-slow")}
						style={{
							height: team.data.score.co4 * 0.8 + "%",
							background:
								team.data.score.co4 < 30
									? "#2d8f19"
									: team.data.score.co4 < 50
									? "#ffde59"
									: team.data.score.co4 < 70
									? "orange"
									: "red"
						}}
					/>
					<div className="team-color fade-animation" />
					<img src={meterImage} className="meter" />
				</div>
			);
		});
	}

	render() {
		return <div className="base-container flx-row score-bord">{this.renderTeams()}</div>;
	}
}

function mapStateToProps(state: AppStore) {
	return {
		data: state.system
	};
}

export default connect(
	mapStateToProps,
	{ dataListener }
)(ScoreContainer);
