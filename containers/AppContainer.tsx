import * as React from "react";
import { Suspense, lazy, Component } from "react";
import { Switch, Route, NavLink } from "react-router-dom";
import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import reducers from "../reducers";
import { Provider } from "react-redux";

const createStoreWithMiddleWare = applyMiddleware(thunk)(createStore);

const BankContainer = lazy(() => import("./BankContainer"));
const ScoreContainer = lazy(() => import("./ScoreContainer"));
const ScoreBordContainer = lazy(() => import("./ScoreBordContainer"));

export default class AppContainer extends Component {
	render() {
		return (
			<Provider store={createStoreWithMiddleWare(reducers)}>
				<Suspense fallback={<div>Loading...</div>}>
					<div className="navig">
						<NavLink exact activeClassName="active" to={"/"}>
							<div>1</div>
						</NavLink>
						<NavLink exact activeClassName="active" to={"/score"}>
							<div>2</div>
						</NavLink>
						<NavLink exact activeClassName="active" to={"/scorebord"}>
							<div>3</div>
						</NavLink>
					</div>
					<Switch>
						<Route path="/scorebord" component={ScoreBordContainer} />
						<Route path="/score" component={ScoreContainer} />
						<Route path="/" component={BankContainer} />
					</Switch>
				</Suspense>
			</Provider>
		);
	}
}
