import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { geolocated } from "react-geolocated";
import { Switch, Route, NavLink } from "react-router-dom";
import { players } from "../Firebase";
import { User } from "firebase";
import { bearing, distance } from "../shared/shared.functions";

const PlayerContainer = React.lazy(() => import("./PlayerContainer"));

type ThisState = {
	lastLat: number;
	lastLong: number;
	scndLastLat: number;
	scndLastLong: number;
	heading: number;
	target: number;
	interval: any;
	counter: number;
};

interface ThisProps {
	isGeolocationAvailable?: boolean;
	isGeolocationEnabled?: boolean;
	coords?: Coordinates;
	auth: User;
}

class GPSContainer extends React.Component<ThisProps, ThisState> {
	componentWillMount() {
		this.setState({
			lastLat: 0,
			lastLong: 0,
			scndLastLat: 0,
			scndLastLong: 0,
			interval: setInterval(() => {
				this.sendCoords();
			}, 10000),
			counter: 0
		});
	}

	componentWillReceiveProps(nextProps) {
		console.log(nextProps);
		if (!_.isEqualWith(nextProps.coords, this.props.coords)) {
			this.setState({
				target: bearing(nextProps.coords.latitude, nextProps.coords.longitude, 52.067439, 4.451487),
				heading: bearing(
					this.state.lastLat,
					this.state.lastLong,
					nextProps.coords.latitude,
					nextProps.coords.longitude
				),
				lastLat: nextProps.coords.latitude,
				lastLong: nextProps.coords.longitude,
				scndLastLat: this.state.lastLat,
				scndLastLong: this.state.lastLong
			});
		}
		console.log(this.state);
	}

	componentWillUnmount() {
		clearInterval(this.state.interval);
	}

	sendCoords() {
		console.log("sendCoords");
		if (this.props.coords && this.props.isGeolocationEnabled) {
			players
				.child(this.props.auth.uid)
				.child("location")
				.update({
					long: this.props.coords.longitude,
					lat: this.props.coords.latitude,
					accuracy: Math.floor(this.props.coords.accuracy)
				});
		}
	}

	clear() {
		clearInterval(this.state.interval);
	}

	reset() {
		this.forceUpdate();
	}

	dialOpacity(coords) {
		if (coords) {
			if (coords.speed < 0.07) {
				return 0;
			} else if (coords.speed > 0.3) {
				return 1;
			} else {
				return coords.speed * 3.3;
			}
		} else {
			return 0;
		}
	}

	render() {
		return (
			<div className="gps-container">
				{!this.props.isGeolocationAvailable ? (
					<div>Deze browser ondersteunt geen GPS</div>
				) : !this.props.isGeolocationEnabled ? (
					<div>GPS is uitgeschakeld</div>
				) : this.props.coords ? (
					<div>GPS actief</div>
				) : (
					<div>Locatie data ophalen&hellip; </div>
				)}
				<br />
				<PlayerContainer coordsInput={this.props.coords} heading={Math.floor(this.state.heading)} />
			</div>
		);
	}
}

export default geolocated({
	positionOptions: {
		enableHighAccuracy: true,
		maximumAge: 5000,
		timeout: Infinity
	},
	watchPosition: true,
	userDecisionTimeout: null,
	suppressLocationOnMount: false
})(GPSContainer);
