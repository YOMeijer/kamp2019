import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { AppStore } from "../reducers";
import { dataListener } from "../actions/SystemActions";
import { SystemState } from "../reducers/SystemReducer";
import { kamp2019 } from "../Firebase";

type ThisState = {
	data: SystemState;
};

type ThisProps = {
	dataListener();
	data: SystemState;
};

class ScoreContainer extends React.Component<ThisProps, ThisState> {
	state = {
		data: null
	};

	componentDidMount() {
		this.props.dataListener();
	}

	static getDerivedStateFromProps(props, state) {
		return state;
	}

	setScore(team: string, type: string) {
		var score = prompt("Score:", "");
		if (score) {
			kamp2019
				.child("teams")
				.child(team)
				.child("score")
				.child(type)
				.set(score);
		}
	}

	renderTeams() {
		return _.map(this.props.data, team => {
			return (
				<div className={"team-score-container " + team.name} key={team.name}>
					<div>{team.name.toUpperCase()}</div>
					<div className="flx-row bank-welp">
						<div> CO2:</div>{" "}
						<button onClick={() => this.setScore(team.name, "co2")} className="btn btn-bank">
							{team.data.score.co2}%
						</button>
					</div>
					<div className="flx-row bank-welp">
						<div> CO4:</div>{" "}
						<button onClick={() => this.setScore(team.name, "co4")} className="btn btn-bank">
							{team.data.score.co4 ? team.data.score.co4 : 0}%
						</button>
					</div>
				</div>
			);
		});
	}

	render() {
		return (
			<div className="base-container flx-col">
				<div className="teams flx-row">{this.renderTeams()}</div>
			</div>
		);
	}
}

function mapStateToProps(state: AppStore) {
	return {
		data: state.system
	};
}

export default connect(
	mapStateToProps,
	{ dataListener }
)(ScoreContainer);
