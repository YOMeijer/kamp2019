import * as React from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { AppStore } from "../reducers";
import { dataListener } from "../actions/SystemActions";
import { SystemState } from "../reducers/SystemReducer";
import { kamp2019 } from "../Firebase";

type ThisState = {
	data: SystemState;
};

type ThisProps = {
	dataListener();
	data: SystemState;
};

class BankContainer extends React.Component<ThisProps, ThisState> {
	state = {
		data: null
	};

	componentDidMount() {
		this.props.dataListener();
	}

	static getDerivedStateFromProps(props, state) {
		console.log(props, state);
		return state;
	}

	deposit(naam: string, team: string, index: number) {
		var bedrag = prompt("Bedrag:", "0");
		if (bedrag) {
			const old = this.props.data[index].data.welpen[naam];
			kamp2019
				.child("teams")
				.child(team)
				.child("welpen")
				.child(naam)
				.set(old + parseInt(bedrag));
		}
	}

	depositAll(team: any, index: number) {
		var bedrag = prompt("Bedrag:", "0");
		if (bedrag) {
			_.map(team.data.welpen, (geld, welp) => {
				kamp2019
					.child("teams")
					.child(team.name)
					.child("welpen")
					.child(welp)
					.set(geld + parseInt(bedrag));
			});
		}
	}

	renderWelpen(welpen, team: string, index: number) {
		return _.map(welpen, (geld, naam) => {
			return (
				<div className="flx-row bank-welp" key={naam}>
					<div>{naam.split("_").join(" ")}</div>
					<button className="btn btn-bank" onClick={() => this.deposit(naam, team, index)}>
						{geld}
					</button>
				</div>
			);
		});
	}

	getTotal(team: any) {
		let total = 0;
		_.map(team.data.welpen, welp => {
			total = total + welp;
		});
		return total;
	}

	renderTeams() {
		return _.map(this.props.data, (team, index) => {
			return (
				<div key={team.name} className={"team-bank-container " + team.name}>
					<div className="flx-row bank-welp">
						<div>{team.name.toUpperCase()}</div>
						<button className="btn btn-bank" onClick={() => this.depositAll(team, parseInt(index))}>
							{this.getTotal(team)}
						</button>
					</div>
					{this.renderWelpen(team.data.welpen, team.name, parseInt(index))}
				</div>
			);
		});
	}

	render() {
		return (
			<div className="base-container flx-col">
				<div className="teams">{this.renderTeams()}</div>
			</div>
		);
	}
}

function mapStateToProps(state: AppStore) {
	return {
		data: state.system
	};
}

export default connect(
	mapStateToProps,
	{ dataListener }
)(BankContainer);
