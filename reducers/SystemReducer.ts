import { SET_DATA } from "../actions/SystemActions";

export const INITITAL_SYSTEM_STATE: SystemState = [];

export interface SystemState {
  [index: number]: { name: string; data: any };
}

export function SystemReducer(
  state: SystemState = INITITAL_SYSTEM_STATE,
  action
) {
  switch (action.type) {
    case SET_DATA:
      return action.payload;
    default:
      return state;
  }
}
