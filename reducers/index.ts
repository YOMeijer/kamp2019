import { combineReducers } from "redux";
import { SystemReducer, SystemState } from "./SystemReducer";

export interface AppStore {
  system: SystemState;
}

const rootReducer = combineReducers({
  system: SystemReducer
});

export default rootReducer;
