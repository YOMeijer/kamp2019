import React, { Suspense, lazy } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import "./styles/style.scss";

const AppContainer = lazy(() => import("./containers/AppContainer.tsx"));

ReactDOM.render(
  <BrowserRouter>
    <Suspense fallback={<div>Loading...</div>}>
      <AppContainer />
    </Suspense>
  </BrowserRouter>,
  document.getElementById("root")
);

///
